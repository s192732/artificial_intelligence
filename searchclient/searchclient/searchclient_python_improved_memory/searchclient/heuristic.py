from abc import ABCMeta, abstractmethod
from state import Level
import sys
import math


class Heuristic(metaclass=ABCMeta):
    def __init__(self, initial_state: 'State'):
        # Here's a chance to pre-process the static parts of the level.
        pass

    #Baseline heuristic function
    def h(self, state: 'State') -> 'int':
        cost = 0
        for i_box, row_boxes in enumerate(state.boxes):
            for j_box, box in enumerate(row_boxes):
                if box != None:
                    coords_goals = Level.goals_coords[box.lower()]
                    i_goal, j_goal = coords_goals[0]
                    cost += abs(i_goal - i_box) + abs(j_box - j_goal)
        return cost

    @abstractmethod
    def f(self, state: 'State') -> 'int':
        pass

    @abstractmethod
    def __repr__(self):
        raise NotImplementedError


class AStar(Heuristic):
    def __init__(self, initial_state: 'State'):
        super().__init__(initial_state)

    def f(self, state: 'State') -> 'int':
        return state.g + self.h(state)

    def __repr__(self):
        return 'A* evaluation'


class WAStar(Heuristic):
    def __init__(self, initial_state: 'State', w: 'int'):
        super().__init__(initial_state)
        self.w = w

    def f(self, state: 'State') -> 'int':
        return state.g + self.w * self.h(state)

    def __repr__(self):
        return 'WA* ({}) evaluation'.format(self.w)


class Greedy(Heuristic):
    def __init__(self, initial_state: 'State'):
        super().__init__(initial_state)

    def f(self, state: 'State') -> 'int':
        return self.h(state)

    def __repr__(self):
        return 'Greedy evaluation'

