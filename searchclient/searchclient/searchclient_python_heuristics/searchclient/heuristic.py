from abc import ABCMeta, abstractmethod
from state import Level
from _collections import defaultdict
import sys

class Graph:
  def __init__(self):
    self.nodes = set()
    self.edges = defaultdict(set)
    self.distances = {}

  def add_node(self, value):
    self.nodes.add(value)

  def add_edge(self, from_node, to_node, distance):
    self.edges[from_node].add(to_node)
    self.edges[to_node].add(from_node)
    self.distances[(from_node, to_node)] = distance
  def coords2id(self, i,j, cols):
      return i*cols + j
  def id2coords(self, id, cols):
      return (int(id/cols),id % cols)

def dijsktra(graph, initial, dest):
  visited = {initial: 0}
  path = {}

  nodes = set(graph.nodes)

  while nodes:
    min_node = None
    for node in nodes:
      if node in visited:
        if min_node is None:
          min_node = node
        elif visited[node] < visited[min_node]:
          min_node = node

    if min_node is None:
      break
    if min_node == dest:
        return visited[min_node]

    nodes.remove(min_node)
    current_weight = visited[min_node]

    for edge in graph.edges[min_node]:
      weight = current_weight + graph.distances[(min_node, edge)]
      if edge not in visited or weight < visited[edge]:
        visited[edge] = weight
        path[edge] = min_node

  return 1000

class Heuristic(metaclass=ABCMeta):
    def __init__(self, initial_state: 'State'):
        #pass
        self.graph = Graph()

        for i in range(Level.rows):
            for j in range(Level.cols):
                if not Level.walls[i][j]:
                    cur_node = self.graph.coords2id(i,j,Level.cols)
                    self.graph.add_node(cur_node)
                    for coord in [(-1,0),(1,0),(0,1),(0,-1)]:
                        k = coord[0]
                        l = coord[1]
                        h = min(Level.rows-1, max(0, i+k))
                        u = min(Level.cols-1, max(0, j+l))
                        if not Level.walls[h][u]:
                            dest_node = self.graph.coords2id(h, u, Level.cols)
                            self.graph.add_node(dest_node)
                            self.graph.add_edge(cur_node, dest_node, 1)
                            #print(str(cur_node) + " ->" + str(dest_node), file=sys.stderr, flush=True)
    #def h(self, state: 'State') -> 'int':
    #    if (state.is_goal_state()):
    #        return 0
    #    else:
    #        distances = state.get_distance_to_goals()
    #        sum = 0
    #        for key in distances:
    #            sum += distances[key]
    #        return sum

    #def h(self, state: 'State') -> 'int':
    #    if (state.is_goal_state()):
    #        return 0
    #    else:
    #        distances = state.get_distance_to_goals()
    #        sum = 0
    #        for key in distances:
    #            for boxes_mindist in distances[key]:
    #                sum += boxes_mindist
    #        return sum

    #def h(self, state: 'State') -> 'int':
    #    if (state.is_goal_state()):
    #        return 0
    #    else:
    #        cost = 0
    #        s_coords = (state.agent_row, state.agent_col)
    #        for box, list_b_coords in state.boxes_coords.items():
    #            for b_coords in list_b_coords:
    #                if Level.goals[b_coords[0]][b_coords[1]] is None or Level.goals[b_coords[0]][b_coords[1]] != box.lower():
    #                    cost += abs(s_coords[0] - b_coords[0]) + abs(s_coords[1] - b_coords[1])
    #        distances = state.get_distance_to_goals()
    #        sum = 0
    #        for key in distances:
    #            for boxes_mindist in distances[key]:
    #                sum += boxes_mindist
    #        return 2*sum + cost

    def h(self, state: 'State') -> 'int':
        if (state.is_goal_state()):
            return 0
        else:
            cost = 0
            s_coords = (state.agent_row, state.agent_col)
            #compute the total cost to move the boxes to their goals
            #print("Computing cost for ("+str(state.agent_row)+","+str(state.agent_col)+")", file=sys.stderr, flush=True)
            for box, list_b_coords in state.boxes_coords.items():
                for b_coords in list_b_coords:
                    if Level.goals[b_coords[0]][b_coords[1]] is None or Level.goals[b_coords[0]][b_coords[1]] != box.lower():
                        cost += abs(s_coords[0] - b_coords[0]) + abs(s_coords[1] - b_coords[1])

            #compute the min path to move the boxes to their goals using dijsktra algorithm
            #print("Computing sum for ("+str(state.agent_row)+","+str(state.agent_col)+") with cost "+str(cost), file=sys.stderr, flush=True)
            for box, list_b_coords in state.boxes_coords.items():
                sum = 0
                
                for b_coords in list_b_coords:

                    min_dist = Level.rows * Level.cols
                    if (len(Level.goals_coords[box.lower()])>10):
                        for g_coords in Level.goals_coords[box.lower()]:
                            dist = abs(g_coords[0] - b_coords[0]) + abs(g_coords[1] - b_coords[1])
                            if dist < min_dist:
                                min_dist = dist
                    else:
                        for g_coords in Level.goals_coords[box.lower()]:
                            #print("Box ("+str(b_coords[0])+","+str(b_coords[1])+") Goal ("+str(g_coords[0])+","+str(g_coords[1])+")", file=sys.stderr, flush=True)
                            start_node = self.graph.coords2id(b_coords[0], b_coords[1], Level.cols)
                            end_node = self.graph.coords2id(g_coords[0], g_coords[1], Level.cols)
                            dist = dijsktra(self.graph, start_node, end_node)
                            #if start_node in self.graph.nodes:
                            #    print(str(start_node)+ " is in the graph "+ str(dist), file=sys.stderr, flush=True)
                            if dist < min_dist:
                                min_dist = dist
                    sum += min_dist

            return 100*sum + cost
    
    @abstractmethod
    def f(self, state: 'State') -> 'int': pass
    
    @abstractmethod
    def __repr__(self): raise NotImplementedError


class AStar(Heuristic):
    def __init__(self, initial_state: 'State'):
        super().__init__(initial_state)

    def f(self, state: 'State') -> 'int':
        return state.g + self.h(state)
    
    def __repr__(self):
        return 'A* evaluation'


class WAStar(Heuristic):
    def __init__(self, initial_state: 'State', w: 'int'):
        super().__init__(initial_state)
        self.w = w
    
    def f(self, state: 'State') -> 'int':
        return state.g + self.w * self.h(state)
    
    def __repr__(self):
        return 'WA* ({}) evaluation'.format(self.w)


class Greedy(Heuristic):
    def __init__(self, initial_state: 'State'):
        super().__init__(initial_state)
    
    def f(self, state: 'State') -> 'int':
        return self.h(state)
    
    def __repr__(self):
        return 'Greedy evaluation'

